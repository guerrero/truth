/*
   Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
 */

#ifndef ANALYSISTOP_TOPPARTONS_CALCThqPARTONHISTORY_H
#define ANALYSISTOP_TOPPARTONS_CALCThqARTONHISTORY_H


// Framework include(s):
#include "TopPartons/CalcTopPartonHistory.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "TopPartons/PartonHistory.h"

// forward declaration(s):
namespace top {
  class TopConfig;
}

namespace top {
  class CalcThqPartonHistory: public CalcTopPartonHistory {
  public:
    explicit CalcThqPartonHistory(const std::string& name);
    virtual ~CalcThqPartonHistory() {}

    struct tH_values {
      //Higgs
      int H_decay_channel;
      TLorentzVector H_p4;

      TLorentzVector H_decay1_p4;
      int H_decay1_pdgId;
      TLorentzVector H_decay2_p4;
      int H_decay2_pdgId;

      //from tau
      TLorentzVector nu_from_Tau1_p4;
      int nu_from_Tau1_pdgId;
      TLorentzVector nu_from_Tau2_p4;
      int nu_from_Tau2_pdgId;
    
      TLorentzVector W_decay1_from_Tau1_p4;
      int W_decay1_from_Tau1_pdgId;
      TLorentzVector W_decay2_from_Tau1_p4;
      int W_decay2_from_Tau1_pdgId;
      TLorentzVector W_decay1_from_Tau2_p4;
      int W_decay1_from_Tau2_pdgId;
      TLorentzVector W_decay2_from_Tau2_p4;
      int W_decay2_from_Tau2_pdgId;

      //from W
      TLorentzVector W_decay1_from_W1_p4;
      int W_decay1_from_W1_pdgId;
      TLorentzVector W_decay1_from_W2_p4;
      int W_decay1_from_W2_pdgId;
      TLorentzVector W_decay2_from_W1_p4;
      int  W_decay2_from_W1_pdgId;
      TLorentzVector  W_decay2_from_W2_p4;
      int  W_decay2_from_W2_pdgId;
      
      //from Z
      TLorentzVector Z_Lepton1_from_Z1_p4;
      int Z_Lepton1_from_Z1_pdgId;
      TLorentzVector Z_Lepton1_from_Z2_p4;
      int Z_Lepton1_from_Z2_pdgId;
      TLorentzVector Z_Lepton2_from_Z1_p4;
      int Z_Lepton2_from_Z1_pdgId;
      TLorentzVector Z_Lepton2_from_Z2_p4;
      int Z_Lepton2_from_Z2_pdgId;

      /////////////////////
      //Bools
      int TauJets1;
      int TauJets2;

      // second b-quark                                                                                                                                                                                                                                                                  
      TLorentzVector secondb_beforeFSR_p4;
      int secondb_beforeFSR_pdgId;
      TLorentzVector secondb_afterFSR_p4;
      int secondb_afterFSR_pdgId;
    } tH;

    //Decay from tau from H
    struct tau_decay{
      TLorentzVector lephad_p4;
      int lephad_pdgId;
      TLorentzVector lepneutrino_p4;
      int lepneutrino_pdgId;
      TLorentzVector tauneutrino_p4;
      int tauneutrino_pdgId;
    } tau;

    //Storing parton history for ttbar resonance analysis
    CalcThqPartonHistory(const CalcThqPartonHistory& rhs) = delete;
    CalcThqPartonHistory(CalcThqPartonHistory&& rhs) = delete;
    CalcThqPartonHistory& operator = (const CalcThqPartonHistory& rhs) = delete;

    void THHistorySaver(const xAOD::TruthParticleContainer* truthParticles, xAOD::PartonHistory* ThqPartonHistory);

    //handle gamma radiation of taus
    const xAOD::TruthParticle* findAfterGamma(const xAOD::TruthParticle* particle);
    const xAOD::TruthParticle* ParticleBeforeRadiation(const xAOD::TruthParticle* particle);
    const xAOD::TruthParticle* ParticleAfterRadiation(const xAOD::TruthParticle* particle);

    ///Store the four-momentum of several particles in the Higgs decay chain
    bool Higgstautau(const xAOD::TruthParticleContainer* truthParticles, int start);

    ///Store the four-momentum of several particles in the Higgs decay chain with 3 light lepton in final state
    bool Higgs(const xAOD::TruthParticleContainer* truthParticles, int start);
    bool HiggsWW(const xAOD::TruthParticleContainer* truthParticles, int start);
    int HiggsZZ(const xAOD::TruthParticleContainer* truthParticles, int start);

    //Store four-momentum of incoming quark
    bool incoming_quark(const xAOD::TruthParticleContainer* truthParticles);

    //Store four-momentum of spectator quark
    bool spectator(const xAOD::TruthParticleContainer* truthParticles);

    //Store four-momentum of bottom quark
    bool secondb(const xAOD::TruthParticleContainer* truthParticles, int start);

    int sign(int a);

    virtual StatusCode execute();
  };
}

#endif
