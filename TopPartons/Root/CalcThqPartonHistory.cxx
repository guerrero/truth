/*
   Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
 */

#include "TopPartons/CalcThqPartonHistory.h"
#include "TopPartons/CalcTopPartonHistory.h"
#include "TopConfiguration/TopConfig.h"


namespace top {
  CalcThqPartonHistory::CalcThqPartonHistory(const std::string& name) : CalcTopPartonHistory(name) {}
  const xAOD::TruthParticle* CalcThqPartonHistory::findAfterGamma(const xAOD::TruthParticle* particle) {
    bool isAfterGamma(false);
    const int particle_ID = particle->pdgId();
    int forLoop;

    while (!isAfterGamma) {
      forLoop = 0;
      for (size_t j = 0; j < particle->nChildren(); j++) {
        const xAOD::TruthParticle* tmp_children = particle->child(j);
        if (tmp_children && tmp_children->pdgId() == particle_ID && tmp_children->pdgId() != 22) {
          particle = particle->child(j);
          forLoop++;
          break;
        }//if
      }//for

      if (forLoop == 0) isAfterGamma = true;
    }//while
    return particle;
  }

  int CalcThqPartonHistory::sign(int a) {
    if (a < 0) {
      return -1;
    } else return 1;
  }

  bool CalcThqPartonHistory::secondb(const xAOD::TruthParticleContainer* truthParticles, int start) {
    for (const xAOD::TruthParticle* particle : *truthParticles) {
      if (particle->pdgId() != start) { continue; }
      // std::cout << "particle (b-quark) :: " << particle->pdgId() << std::endl;

      for (size_t i=0; i< particle->nParents(); i++) {
        const xAOD::TruthParticle* parent = particle->parent(i);
	// std::cout << "- parent :: " << parent->pdgId() << std::endl;

        if (parent->pdgId() == 21) {

	  // for (size_t j = 0; j < particle->nChildren(); j++) {
	  //   const xAOD::TruthParticle* tmp_children = particle->child(j);
	  //   std::cout << "  - child :: " << tmp_children->pdgId() << std::endl;
	  // }

          const xAOD::TruthParticle* secondb_beforeFSR = CalcThqPartonHistory::ParticleBeforeRadiation(particle);
          tH.secondb_beforeFSR_p4 = secondb_beforeFSR->p4();
          //tH.secondb_beforeFSR_p4.Print();
          tH.secondb_beforeFSR_pdgId = secondb_beforeFSR->pdgId();

          const xAOD::TruthParticle* secondb_afterFSR = CalcThqPartonHistory::ParticleAfterRadiation(particle);
          tH.secondb_afterFSR_p4 = secondb_afterFSR->p4();
          //tH.secondb_afterFSR_p4.Print();
          tH.secondb_afterFSR_pdgId = secondb_afterFSR->pdgId();

	  //std::cout << "- this is the second b-quark" << std::endl;

          return true;
        }
      }
    }
    return false;
  }

  bool CalcThqPartonHistory::spectator(const xAOD::TruthParticleContainer* truthParticles) {



    return false;
  }

  const xAOD::TruthParticle* CalcThqPartonHistory::ParticleAfterRadiation(const xAOD::TruthParticle* particle) {
    bool isAfterFSR(false);
    const int particle_ID = particle->pdgId();
    int forLoop  = 0;
    while(!isAfterFSR){
      forLoop  = 0;
      for (size_t j=0; j< particle->nChildren(); j++ ) {
        const xAOD::TruthParticle* tmp_children = particle->child(j);
        if (tmp_children && tmp_children->pdgId()==particle_ID){
          particle = particle->child(j);
          forLoop++;
          break;
        }
      }
      if (forLoop == 0) { isAfterFSR = true; }
    }
    return particle;
  }

  const xAOD::TruthParticle* CalcThqPartonHistory::ParticleBeforeRadiation(const xAOD::TruthParticle* particle) {
    bool skipit = true;
    const int particle_ID = particle->pdgId();
    int forLoop;
    int limit = 0;
    while(skipit && limit < 2000){
      forLoop = 0;
      for (size_t i = 0; i < particle->nParents(); i++) {
        const xAOD::TruthParticle* parent = particle->parent(i);
        if (parent && parent->pdgId() == particle_ID) {
          particle = particle->parent(i);
          forLoop++;
          break;
        }
      }
      if (forLoop == 0)skipit = false;
      limit++;
    }
    if(limit == 2000){std::cout<<"WARNING: Particle before radiation is not found"<<std::endl;}
    return particle;
  }

  bool CalcThqPartonHistory::HiggsWW(const xAOD::TruthParticleContainer* truthParticles, int start) {
    bool has_WtoLepton = false;
    bool tau_decay = false;
    for (const xAOD::TruthParticle* particle : *truthParticles) {
      if (particle->pdgId() != start || particle->nChildren() != 2) {
        continue;
      }
      //std::cout << "TESTPABLO  ->   Start"  << std::endl;
      //std::cout << "TESTPABLO  ->   Higgs :: " << particle->pdgId()  << std::endl;
      for (size_t k = 0; k < particle->nChildren(); k++) {
	const xAOD::TruthParticle* HiggsChildren = particle->child(k);
	if (fabs(HiggsChildren->pdgId()) == 24) {// demanding W as children
	  const xAOD::TruthParticle* Wboson_1 = CalcThqPartonHistory::ParticleBeforeRadiation(HiggsChildren);
	  if (k == 0) {
            tH.H_decay1_p4 = Wboson_1->p4();
            tH.H_decay1_pdgId = Wboson_1->pdgId();
          }else{
	    tH.H_decay2_p4 = Wboson_1->p4();
            tH.H_decay2_pdgId = Wboson_1->pdgId();
          }
	  const xAOD::TruthParticle* Wboson = CalcThqPartonHistory::findAfterGamma(HiggsChildren);
	  //std::cout << "TESTPABLO  ->   W_"<< k+1 << " :: "<< HiggsChildren->pdgId()  << std::endl;
	  //for (size_t e = 0; e < Wboson->nChildren(); ++e) {
          //  const xAOD::TruthParticle* WChild = Wboson->child(e);
	  //  std::cout << "TESTPABLO  ->    W_"<< k+1 << " :: "<< HiggsChildren->pdgId()  << ", has  child_" << e << " :: " << WChild->pdgId() << std::endl;
	  //}
	  for (size_t q = 0; q < Wboson->nChildren(); ++q) {
	    const xAOD::TruthParticle* wChildren = Wboson->child(q);
	    if (fabs(wChildren->pdgId()) >= 11 && fabs(wChildren->pdgId()) <= 16) { //light leptons or neutrinos
	      has_WtoLepton = true;
	      if (fabs(wChildren->pdgId()) == 11 || fabs(wChildren->pdgId()) == 13) { //electron or muon                                                                                                                                                                   
		if (k == 0) {
		  tH.W_decay1_from_W1_p4 = wChildren->p4();
		  tH.W_decay1_from_W1_pdgId = wChildren->pdgId();
		} else {
		  tH.W_decay1_from_W2_p4 = wChildren->p4();
		  tH.W_decay1_from_W2_pdgId = wChildren->pdgId();
		}
	      } 
	      else if (fabs(wChildren->pdgId()) == 12 || fabs(wChildren->pdgId()) == 14 || fabs(wChildren->pdgId()) == 16) { // electron or muon neutrino
		if (k == 0) {
		  tH.W_decay2_from_W1_p4 = wChildren->p4();
		  tH.W_decay2_from_W1_pdgId = wChildren->pdgId();
		} else {
		  tH.W_decay2_from_W2_p4 = wChildren->p4();
		  tH.W_decay2_from_W2_pdgId = wChildren->pdgId();
		}
	      }
	      else if (fabs(wChildren->pdgId()) == 15 ){
		if (k == 0) {
		  tau_decay=CalcTopPartonHistory::TauDecay(wChildren,15,tH.W_decay1_from_W1_p4,tau.lepneutrino_p4,tau.tauneutrino_p4,tH.W_decay1_from_W1_pdgId,tau.lepneutrino_pdgId,tau.tauneutrino_pdgId);
		  if(!tau_decay) tH.W_decay1_from_W1_pdgId = -9999;
		}else{
		  tau_decay=CalcTopPartonHistory::TauDecay(wChildren,15,tH.W_decay1_from_W2_p4,tau.lepneutrino_p4,tau.tauneutrino_p4,tH.W_decay1_from_W2_pdgId,tau.lepneutrino_pdgId,tau.tauneutrino_pdgId);
                  if(!tau_decay) tH.W_decay1_from_W2_pdgId = -9999;
		}
	      }

	    }else{
	    has_WtoLepton = false;
	    break;
	    }
	  } // End of loop over the W children
	}
      } // End of loop over the W
      //if (has_WtoLepton){
      //  std::cout << "TESTPABLO  ->   Output:"<< std::endl;
      //  std::cout << "TESTPABLO  ->    tH.W_decay1_from_W1_pdgId  :: " << tH.W_decay1_from_W1_pdgId << std::endl;
      //  std::cout << "TESTPABLO  ->    tH.W_decay2_from_W1_pdgId  :: " << tH.W_decay2_from_W1_pdgId << std::endl;
      //  std::cout << "TESTPABLO  ->    tH.W_decay1_from_W2_pdgId  :: " << tH.W_decay1_from_W2_pdgId << std::endl;
      //  std::cout << "TESTPABLO  ->    tH.W_decay2_from_W2_pdgId  :: " << tH.W_decay2_from_W2_pdgId << std::endl;
      //}

    }
    return has_WtoLepton;
  }

  int CalcThqPartonHistory::HiggsZZ(const xAOD::TruthParticleContainer* truthParticles, int start) {
    // bool has_ZtoLepton = false;
    int Number_Z = 0;
    bool tau_decay = false;
    for (const xAOD::TruthParticle* particle : *truthParticles) {
      if (particle->pdgId() != start || particle->nChildren() != 2) {
        continue;
      }
      for (size_t k = 0; k < particle->nChildren(); k++) {
	const xAOD::TruthParticle* HiggsChildren = particle->child(k);
	if (fabs(HiggsChildren->pdgId()) == 23) {// demanding Z as children               
	  if (k == 0) {
            tH.H_decay1_p4 = HiggsChildren->p4();
            tH.H_decay1_pdgId = HiggsChildren->pdgId();
          }else{
            tH.H_decay2_p4 = HiggsChildren->p4();
            tH.H_decay2_pdgId = HiggsChildren->pdgId();
          }

	  const xAOD::TruthParticle* Zboson = CalcThqPartonHistory::findAfterGamma(HiggsChildren);
	  for (size_t q = 0; q < Zboson->nChildren(); ++q) {
	    const xAOD::TruthParticle* zChildren = Zboson->child(q);
	    if (fabs(zChildren->pdgId()) == 11 || fabs(zChildren->pdgId()) == 13 || fabs(zChildren->pdgId()) == 15){ //electron or muon
	      // has_ZtoLepton = true;
	      if (k== 0 && q == 0) {
		if (fabs(zChildren->pdgId()) == 11 || fabs(zChildren->pdgId()) == 13 ){
		  tH.Z_Lepton1_from_Z1_p4 = zChildren->p4();
		  tH.Z_Lepton1_from_Z1_pdgId = zChildren->pdgId();
		}
		if (fabs(zChildren->pdgId()) == 15 ){
		  tau_decay=CalcTopPartonHistory::TauDecay(zChildren,15,tH.Z_Lepton1_from_Z1_p4,tau.lepneutrino_p4,tau.tauneutrino_p4,tH.Z_Lepton1_from_Z1_pdgId,tau.lepneutrino_pdgId,tau.tauneutrino_pdgId);
		  if(!tau_decay) tH.Z_Lepton1_from_Z1_pdgId = -9999;
		}
		Number_Z = 1;
	      } else if (k== 0 && q == 1){
		if (fabs(zChildren->pdgId()) == 11 || fabs(zChildren->pdgId()) == 13 ){
		  tH.Z_Lepton2_from_Z1_p4 = zChildren->p4();
		  tH.Z_Lepton2_from_Z1_pdgId = zChildren->pdgId();
		}
		if (fabs(zChildren->pdgId()) == 15 ){
		  tau_decay=CalcTopPartonHistory::TauDecay(zChildren,15,tH.Z_Lepton2_from_Z1_p4,tau.lepneutrino_p4,tau.tauneutrino_p4,tH.Z_Lepton2_from_Z1_pdgId,tau.lepneutrino_pdgId,tau.tauneutrino_pdgId);
		  if(!tau_decay) tH.Z_Lepton2_from_Z1_pdgId = -9999;
		}
	      } else if (k== 1 && q == 0) {
		if (fabs(zChildren->pdgId()) == 11 || fabs(zChildren->pdgId()) == 13 ){
		  tH.Z_Lepton1_from_Z2_p4 = zChildren->p4();
		  tH.Z_Lepton1_from_Z2_pdgId = zChildren->pdgId();
		}
		if (fabs(zChildren->pdgId()) == 15 ){
		  tau_decay=CalcTopPartonHistory::TauDecay(zChildren,15,tH.Z_Lepton1_from_Z2_p4,tau.lepneutrino_p4,tau.tauneutrino_p4,tH.Z_Lepton1_from_Z2_pdgId,tau.lepneutrino_pdgId,tau.tauneutrino_pdgId);
		  if(!tau_decay) tH.Z_Lepton1_from_Z2_pdgId = -9999;
		}
		if(Number_Z == 1){
		  Number_Z = 3;
		}else{
		  Number_Z = 2;
		}
	      }else if (k== 1 && q == 1) {
		if (fabs(zChildren->pdgId()) == 11 || fabs(zChildren->pdgId()) == 13 ){
		  tH.Z_Lepton2_from_Z2_p4 = zChildren->p4();
		  tH.Z_Lepton2_from_Z2_pdgId = zChildren->pdgId();
		}
		if (fabs(zChildren->pdgId()) == 15 ){
		  tau_decay=CalcTopPartonHistory::TauDecay(zChildren,15,tH.Z_Lepton2_from_Z2_p4,tau.lepneutrino_p4,tau.tauneutrino_p4,tH.Z_Lepton2_from_Z2_pdgId,tau.lepneutrino_pdgId,tau.tauneutrino_pdgId);
		  if(!tau_decay) tH.Z_Lepton2_from_Z2_pdgId = -9999;
		}
	      }
	    }
	  }// End of loop over Z children
	}
      }
    }
    return Number_Z;
  }
  bool CalcThqPartonHistory::Higgstautau(const xAOD::TruthParticleContainer* truthParticles, int start) {
    bool has_Higgs = false;
    bool has_tau1_neutrino = false;
    bool has_tau2_neutrino = false;
    bool hadr_tau1 = false;
    bool hadr_tau2 = false;

    for (const xAOD::TruthParticle* particle : *truthParticles) {
      if (particle->pdgId() != start || particle->nChildren() != 2) {
        continue;
      }
      ANA_MSG_DEBUG (" ");
      ANA_MSG_DEBUG ("-- Higgs with two children -- ");
      tH.H_p4 = particle->p4();
      has_Higgs = true;
      for (size_t k = 0; k < particle->nChildren(); k++) {
        const xAOD::TruthParticle* HiggsChildren = particle->child(k);
        if (fabs(HiggsChildren->pdgId()) == 15) {// demanding tau-leptons as childen

	  const xAOD::TruthParticle* tau_1 = CalcThqPartonHistory::ParticleBeforeRadiation(HiggsChildren);

          if (k == 0) {
            ANA_MSG_DEBUG ("First Tau:");
            tH.H_decay1_p4 = tau_1->p4();
            tH.H_decay1_pdgId = tau_1->pdgId();
          } else {
            ANA_MSG_DEBUG ("Second Tau:");
            tH.H_decay2_p4 = tau_1->p4();
            tH.H_decay2_pdgId = tau_1->pdgId();
          }
          const xAOD::TruthParticle* tau = CalcThqPartonHistory::findAfterGamma(HiggsChildren);
	  for (size_t q = 0; q < tau->nChildren(); ++q) {
            const xAOD::TruthParticle* tauChildren = tau->child(q);
            if (fabs(tauChildren->pdgId()) == 16) {// tau neutrino                                                                                                                                                                                                             
              if (k == 0) {
                ANA_MSG_DEBUG (" First Tau has a Neutrino child");
		tH.nu_from_Tau1_p4 = tauChildren->p4();
                tH.nu_from_Tau1_pdgId = tauChildren->pdgId();
                has_tau1_neutrino = true;
              } else {
                ANA_MSG_DEBUG (" Second Tau has a Neutrino child");
                tH.nu_from_Tau2_p4 = tauChildren->p4();
                tH.nu_from_Tau2_pdgId = tauChildren->pdgId();
                has_tau2_neutrino = true;
              }
            } else if (fabs(tauChildren->pdgId()) >= 11 && fabs(tauChildren->pdgId()) <= 14) { //light leptons                                                                                                                                                                 
              if (fabs(tauChildren->pdgId()) == 11 || fabs(tauChildren->pdgId()) == 13) { //electron or muon                                                                                                                                                                  
                if (k == 0) {
                  ANA_MSG_DEBUG (" First Tau has an elctron or muon child");
                  tH.W_decay1_from_Tau1_p4 = tauChildren->p4();
                  tH.W_decay1_from_Tau1_pdgId = tauChildren->pdgId();
                } else {
                  ANA_MSG_DEBUG (" Second Tau has an elctron or muon child");
                  tH.W_decay1_from_Tau2_p4 = tauChildren->p4();
                  tH.W_decay1_from_Tau2_pdgId = tauChildren->pdgId();
                }
              } else if (fabs(tauChildren->pdgId()) == 12 || fabs(tauChildren->pdgId()) == 14) { // electron or muon                                                                                                                                                          
                if (k == 0) {
                  ANA_MSG_DEBUG (" First Tau has an elctron neutrino or muon neutrino child");
                  tH.W_decay2_from_Tau1_p4 = tauChildren->p4();
                  tH.W_decay2_from_Tau1_pdgId = tauChildren->pdgId();
                } else {
                  ANA_MSG_DEBUG (" Second Tau has an elctron neutrino or muon neutrino child");
                  tH.W_decay2_from_Tau2_p4 = tauChildren->p4();
                  tH.W_decay2_from_Tau2_pdgId = tauChildren->pdgId();
                }
              }
            } else { // if a particle passes the criteria above, it has to be a hadron.                                                                                                                                                                                         
              if (k == 0) {
                ANA_MSG_DEBUG (" First Tau decays hadronically");
                hadr_tau1 = true;
              } else {
                ANA_MSG_DEBUG (" Second Tau decays hadronically");
                hadr_tau2 = true;
	      }
            }// else                                                                                                                                                                                                                                                            
          } // for                                                                                                                                                                                                                                                              
        }//if                                                                                                                                                                                                                                                                   
      } //for                                                                                                                                                                                                                                                                   
    }

    if (has_Higgs && has_tau1_neutrino && has_tau2_neutrino) {
      ANA_MSG_DEBUG ("H -> tau tau    event ");
      if (hadr_tau1 && !hadr_tau2) { //convention: store hadr. decaying W-Boson as Wdecay1, set all parameters of Wdecay2 to 0.                                                                                                                                                 
	ANA_MSG_DEBUG (" Only first Tau decays hadronically: We look for a jet from it");
	ANA_MSG_DEBUG (" Channel: H --> Tau_had + Tau_lep");
	tH.W_decay1_from_Tau1_p4 = tH.H_decay1_p4 - tH.nu_from_Tau1_p4;
	tH.W_decay1_from_Tau1_pdgId = 15 * sign(tH.nu_from_Tau1_pdgId);
	tH.W_decay2_from_Tau1_p4 = {
	  0, 0, 0, 0
	};
	tH.W_decay2_from_Tau1_pdgId = 0;
	tH.TauJets1 = 1;
	tH.TauJets2 = 0;
	return true;
      }
      else if (hadr_tau2 && !hadr_tau1) {
	ANA_MSG_DEBUG (" Only second Tau decays hadronically: We look for one jet from it");
	ANA_MSG_DEBUG (" Channel: H --> Tau_lep + Tau_had");
	tH.W_decay1_from_Tau2_p4 = tH.H_decay2_p4 - tH.nu_from_Tau2_p4;
	tH.W_decay1_from_Tau2_pdgId = 15 * sign(tH.nu_from_Tau2_pdgId);
	tH.W_decay2_from_Tau2_p4 = {
	  0, 0, 0, 0
	};
	tH.W_decay2_from_Tau2_pdgId = 0;
	tH.TauJets2 = 1;
	tH.TauJets1 = 0;
	return true;
      }
      else if (hadr_tau2 && hadr_tau1) {
	ANA_MSG_DEBUG (" Both Taus decays hadronically: We look for two jets ");
	tH.W_decay1_from_Tau1_p4 = tH.H_decay1_p4 - tH.nu_from_Tau1_p4;
	tH.W_decay1_from_Tau1_pdgId = 15 * sign(tH.nu_from_Tau1_pdgId);
	tH.W_decay2_from_Tau1_p4 = {
	  0, 0, 0, 0
	};
	tH.W_decay1_from_Tau2_p4 = tH.H_decay2_p4 - tH.nu_from_Tau2_p4;
	tH.W_decay1_from_Tau2_pdgId = 15 * sign(tH.nu_from_Tau2_pdgId);
	tH.W_decay2_from_Tau2_p4 = {
	  0, 0, 0, 0
	};
	tH.W_decay2_from_Tau1_pdgId = 0;
	tH.W_decay2_from_Tau2_pdgId = 0;
	tH.TauJets2 = 1;
	tH.TauJets1 = 1;
	return true;
      }else if (!hadr_tau2 && !hadr_tau1) {
	ANA_MSG_DEBUG (" Both Tau decay leptonically: No jets");
        ANA_MSG_DEBUG (" Channel: H --> Tau_lep + Tau_lep");
        tH.TauJets2 = 0;
	tH.TauJets1 = 0;
	return true;
      };

    }
    ANA_MSG_DEBUG ("RETURN FALSE");
    ANA_MSG_DEBUG (" Taus without neutrinos --> RETURN FALSE");
    
    return false;
  }
  

  bool CalcThqPartonHistory::Higgs(const xAOD::TruthParticleContainer* truthParticles, int start) {
    bool has_Higgs = false;
    int Higgs_decay = 0;
    for (const xAOD::TruthParticle* particle : *truthParticles) {

      if (particle->pdgId() == start && particle->nChildren() == 2) {
	//std::cout << "particle (Higgs) :: " << particle->pdgId() << std::endl;
	// for (size_t i=0; i< particle->nParents(); i++) {                                                                 
	//   const xAOD::TruthParticle* parent = particle->parent(i);
	//   std::cout << "- parent :: " << parent->pdgId() << std::endl;
	// }
	Higgs_decay = 0;
	for (size_t j = 0; j < particle->nChildren(); j++) {
	  const xAOD::TruthParticle* children = particle->child(j);
	  //std::cout << "  - child :: " << children->pdgId() << std::endl;
	  Higgs_decay += fabs(children->pdgId());
	}
      }
      
      if (particle->pdgId() != start || particle->nChildren() != 2) {
        continue;
      }
      has_Higgs = true;
      //std::cout << "  - Higgs decay :: " << Higgs_decay << std::endl;
      tH.H_decay_channel = Higgs_decay;
    }
    return has_Higgs;
  }
  
  
  void CalcThqPartonHistory::THHistorySaver(const xAOD::TruthParticleContainer* truthParticles,
                                            xAOD::PartonHistory* ThqPartonHistory) {
    ThqPartonHistory->IniVarThqML();
    TLorentzVector t_before, t_after, t_after_SC;
    TLorentzVector Wp;
    TLorentzVector b;
    TLorentzVector WpDecay1;
    TLorentzVector WpDecay2;
    int WpDecay1_pdgId;
    int WpDecay2_pdgId;
    bool event_HiggsZZ;

    bool event_top = CalcTopPartonHistory::topWb(truthParticles, 6, t_before, t_after, Wp, b, WpDecay1, WpDecay1_pdgId, WpDecay2, WpDecay2_pdgId);
    bool event_top_SC = CalcTopPartonHistory::topAfterFSR_SC(truthParticles, 6, t_after_SC);
    bool event_topbar = CalcTopPartonHistory::topWb(truthParticles, -6, t_before, t_after, Wp, b, WpDecay1, WpDecay1_pdgId, WpDecay2, WpDecay2_pdgId);
    bool event_topbar_SC = CalcTopPartonHistory::topAfterFSR_SC(truthParticles, -6, t_after_SC);

    // std::cout << "event_top : " << event_top << std::endl;
    // std::cout << "event_topbar : " << event_topbar << std::endl;

    bool event_Higgs = CalcThqPartonHistory::Higgs(truthParticles, 25);
    //std::cout << event_Higgs << std::endl;
    //std::cout << std::endl;

    bool event_secondb = false;
    bool event_secondbbar = false;
    if (event_topbar) {
      event_secondb = event_topbar && CalcThqPartonHistory::secondb(truthParticles, 5);
      CalcThqPartonHistory::spectator(truthParticles);
    }
    if (event_top) {
      event_secondbbar = event_top && CalcThqPartonHistory::secondb(truthParticles, -5);
      CalcThqPartonHistory::spectator(truthParticles);
    }

    // std::cout << "event_topbar: " << event_topbar << " :: event_secondb: " << event_secondb << std::endl;
    // std::cout << "event_top: " << event_top << " :: event_secondbbar: " << event_secondbbar << std::endl;

    int Number_HiggsZZ = CalcThqPartonHistory::HiggsZZ(truthParticles, 25);
    bool event_Higgstautau = CalcThqPartonHistory::Higgstautau(truthParticles, 25);
    bool event_HiggsWW = CalcThqPartonHistory::HiggsWW(truthParticles, 25);
    // std::cout << std::endl;

    if(Number_HiggsZZ == 1 || Number_HiggsZZ == 2 || Number_HiggsZZ == 3) {
      event_HiggsZZ = true;
    }
    if (event_Higgs) {
      ThqPartonHistory->auxdecor< int >("MC_H_decay") = tH.H_decay_channel;

      if ((event_top && event_secondbbar) || (event_topbar && event_secondb)) {

        //second b-quark
	ThqPartonHistory->auxdecor< float >("MC_secondb_beforeFSR_m") = tH.secondb_beforeFSR_p4.M();
	ThqPartonHistory->auxdecor< float >("MC_secondb_beforeFSR_pt") = tH.secondb_beforeFSR_p4.Pt();
	ThqPartonHistory->auxdecor< float >("MC_secondb_beforeFSR_phi") = tH.secondb_beforeFSR_p4.Phi();
	ThqPartonHistory->auxdecor< int >("MC_secondb_beforeFSR_pdgId") = tH.secondb_beforeFSR_pdgId;
	fillEtaBranch(ThqPartonHistory, "MC_secondb_beforeFSR_eta", tH.secondb_beforeFSR_p4);
	
	ThqPartonHistory->auxdecor< float >("MC_secondb_afterFSR_m") = tH.secondb_afterFSR_p4.M();
	ThqPartonHistory->auxdecor< float >("MC_secondb_afterFSR_pt") = tH.secondb_afterFSR_p4.Pt();
	ThqPartonHistory->auxdecor< float >("MC_secondb_afterFSR_phi") = tH.secondb_afterFSR_p4.Phi();
	ThqPartonHistory->auxdecor< int >("MC_secondb_afterFSR_pdgId") = tH.secondb_afterFSR_pdgId;
	fillEtaBranch(ThqPartonHistory, "MC_secondb_afterFSR_eta", tH.secondb_afterFSR_p4);      	

	// top quark and its decays
        ThqPartonHistory->auxdecor< float >("MC_t_beforeFSR_m") = t_before.M();
        ThqPartonHistory->auxdecor< float >("MC_t_beforeFSR_pt") = t_before.Pt();
        ThqPartonHistory->auxdecor< float >("MC_t_beforeFSR_phi") = t_before.Phi();
        fillEtaBranch(ThqPartonHistory, "MC_t_beforeFSR_eta", t_before);

        ThqPartonHistory->auxdecor< float >("MC_t_afterFSR_m") = t_after.M();
        ThqPartonHistory->auxdecor< float >("MC_t_afterFSR_pt") = t_after.Pt();
        ThqPartonHistory->auxdecor< float >("MC_t_afterFSR_phi") = t_after.Phi();
        fillEtaBranch(ThqPartonHistory, "MC_t_afterFSR_eta", t_after);

        if (event_top_SC || event_topbar_SC) {
          ThqPartonHistory->auxdecor< float >("MC_t_afterFSR_SC_m") = t_after_SC.M();
          ThqPartonHistory->auxdecor< float >("MC_t_afterFSR_SC_pt") = t_after_SC.Pt();
          ThqPartonHistory->auxdecor< float >("MC_t_afterFSR_SC_phi") = t_after_SC.Phi();
          fillEtaBranch(ThqPartonHistory, "MC_t_afterFSR_SC_eta", t_after_SC);
        }

        ThqPartonHistory->auxdecor< float >("MC_W_from_t_m") = Wp.M();
        ThqPartonHistory->auxdecor< float >("MC_W_from_t_pt") = Wp.Pt();
        ThqPartonHistory->auxdecor< float >("MC_W_from_t_phi") = Wp.Phi();
        fillEtaBranch(ThqPartonHistory, "MC_W_from_t_eta", Wp);

        ThqPartonHistory->auxdecor< float >("MC_b_from_t_m") = b.M();
        ThqPartonHistory->auxdecor< float >("MC_b_from_t_pt") = b.Pt();
        ThqPartonHistory->auxdecor< float >("MC_b_from_t_phi") = b.Phi();
        fillEtaBranch(ThqPartonHistory, "MC_b_from_t_eta", b);

        ThqPartonHistory->auxdecor< float >("MC_Wdecay1_from_t_m") = WpDecay1.M();
        ThqPartonHistory->auxdecor< float >("MC_Wdecay1_from_t_pt") = WpDecay1.Pt();
        ThqPartonHistory->auxdecor< float >("MC_Wdecay1_from_t_phi") = WpDecay1.Phi();
        ThqPartonHistory->auxdecor< int >("MC_Wdecay1_from_t_pdgId") = WpDecay1_pdgId;
        fillEtaBranch(ThqPartonHistory, "MC_Wdecay1_from_t_eta", WpDecay1);

        ThqPartonHistory->auxdecor< float >("MC_Wdecay2_from_t_m") = WpDecay2.M();
        ThqPartonHistory->auxdecor< float >("MC_Wdecay2_from_t_pt") = WpDecay2.Pt();
        ThqPartonHistory->auxdecor< float >("MC_Wdecay2_from_t_phi") = WpDecay2.Phi();
        ThqPartonHistory->auxdecor< int >("MC_Wdecay2_from_t_pdgId") = WpDecay2_pdgId;
        fillEtaBranch(ThqPartonHistory, "MC_Wdecay2_from_t_eta", WpDecay2);

        //Higgs-Variables
        ThqPartonHistory->auxdecor< float >("MC_H_m") = tH.H_p4.M();
        ThqPartonHistory->auxdecor< float >("MC_H_pt") = tH.H_p4.Pt();
        ThqPartonHistory->auxdecor< float >("MC_H_phi") = tH.H_p4.Phi();
        fillEtaBranch(ThqPartonHistory, "MC_H_eta", tH.H_p4);

	//Higgs decay
	if(event_Higgstautau ||event_HiggsWW ||event_HiggsZZ) {
	  ThqPartonHistory->auxdecor< float >("MC_H_decay1_m") = tH.H_decay1_p4.M();
	  ThqPartonHistory->auxdecor< float >("MC_H_decay1_pt") = tH.H_decay1_p4.Pt();
	  ThqPartonHistory->auxdecor< float >("MC_H_decay1_phi") = tH.H_decay1_p4.Phi();
	  ThqPartonHistory->auxdecor< int >("MC_H_decay1_pdgId") = tH.H_decay1_pdgId;
	  fillEtaBranch(ThqPartonHistory, "MC_H_decay1_eta", tH.H_decay1_p4);   
   
	  ThqPartonHistory->auxdecor< float >("MC_H_decay2_m") = tH.H_decay2_p4.M();
	  ThqPartonHistory->auxdecor< float >("MC_H_decay2_pt") = tH.H_decay2_p4.Pt();
	  ThqPartonHistory->auxdecor< float >("MC_H_decay2_phi") = tH.H_decay2_p4.Phi();
	  ThqPartonHistory->auxdecor< int >("MC_H_decay2_pdgId") = tH.H_decay2_pdgId;
	  fillEtaBranch(ThqPartonHistory, "MC_H_decay2_eta", tH.H_decay2_p4);
	}

        //First Tau (Tau1)
	if(event_Higgstautau) {

	//Neutrino from first tau
	  ThqPartonHistory->auxdecor< float >("MC_nu_from_Tau1_m") = tH.nu_from_Tau1_p4.M();
	  ThqPartonHistory->auxdecor< float >("MC_nu_from_Tau1_pt") = tH.nu_from_Tau1_p4.Pt();
	  ThqPartonHistory->auxdecor< float >("MC_nu_from_Tau1_phi") = tH.nu_from_Tau1_p4.Phi();
	  ThqPartonHistory->auxdecor< int >("MC_nu_from_Tau1_pdgId") = tH.nu_from_Tau1_pdgId;
	  fillEtaBranch(ThqPartonHistory, "MC_nu_from_Tau1_eta", tH.nu_from_Tau1_p4);

	//First hadronic decay product from the W-Boson from Tau1                                                                                                                                                                                                          
	  ThqPartonHistory->auxdecor< float >("MC_W_decay1_from_Tau1_m") = tH.W_decay1_from_Tau1_p4.M();
	  ThqPartonHistory->auxdecor< float >("MC_W_decay1_from_Tau1_pt") = tH.W_decay1_from_Tau1_p4.Pt();
	  ThqPartonHistory->auxdecor< float >("MC_W_decay1_from_Tau1_phi") = tH.W_decay1_from_Tau1_p4.Phi();
	  ThqPartonHistory->auxdecor< int >("MC_W_decay1_from_Tau1_pdgId") = tH.W_decay1_from_Tau1_pdgId;
	  fillEtaBranch(ThqPartonHistory, "MC_W_decay1_from_Tau1_eta", tH.W_decay1_from_Tau1_p4);

        //Second hadronic decay product from the W-Boson from Tau1                                                                                                                                                                                                      
	  ThqPartonHistory->auxdecor< float >("MC_W_decay2_from_Tau1_m") = tH.W_decay2_from_Tau1_p4.M();
	  ThqPartonHistory->auxdecor< float >("MC_W_decay2_from_Tau1_pt") = tH.W_decay2_from_Tau1_p4.Pt();
	  ThqPartonHistory->auxdecor< float >("MC_W_decay2_from_Tau1_phi") = tH.W_decay2_from_Tau1_p4.Phi();
	  ThqPartonHistory->auxdecor< int >("MC_W_decay2_from_Tau1_pdgId") = tH.W_decay2_from_Tau1_pdgId;
	  fillEtaBranch(ThqPartonHistory, "MC_W_decay2_from_Tau1_eta", tH.W_decay2_from_Tau1_p4);
	
        //Second Tau (Tau2)

        //neutrino from second Tau (Tau2)
	  ThqPartonHistory->auxdecor< float >("MC_nu_from_Tau2_m") = tH.nu_from_Tau2_p4.M();
	  ThqPartonHistory->auxdecor< float >("MC_nu_from_Tau2_pt") = tH.nu_from_Tau2_p4.Pt();
	  ThqPartonHistory->auxdecor< float >("MC_nu_from_Tau2_phi") = tH.nu_from_Tau2_p4.Phi();
	  ThqPartonHistory->auxdecor< int >("MC_nu_from_Tau2_pdgId") = tH.nu_from_Tau2_pdgId;
	  fillEtaBranch(ThqPartonHistory, "MC_nu_from_Tau2_eta", tH.nu_from_Tau2_p4);

        //First leptonic decay product from the W-Boson from Tau2
	  ThqPartonHistory->auxdecor< float >("MC_W_decay1_from_Tau2_m") = tH.W_decay1_from_Tau2_p4.M();
	  ThqPartonHistory->auxdecor< float >("MC_W_decay1_from_Tau2_pt") = tH.W_decay1_from_Tau2_p4.Pt();
	  ThqPartonHistory->auxdecor< float >("MC_W_decay1_from_Tau2_phi") = tH.W_decay1_from_Tau2_p4.Phi();
	  ThqPartonHistory->auxdecor< int >("MC_W_decay1_from_Tau2_pdgId") = tH.W_decay1_from_Tau2_pdgId;
	  fillEtaBranch(ThqPartonHistory, "MC_W_decay1_from_Tau2_eta", tH.W_decay1_from_Tau2_p4);

        //Second leptonic decay product from the W-Boson from Tau2
	  ThqPartonHistory->auxdecor< float >("MC_W_decay2_from_Tau2_m") = tH.W_decay2_from_Tau2_p4.M();
	  ThqPartonHistory->auxdecor< float >("MC_W_decay2_from_Tau2_pt") = tH.W_decay2_from_Tau2_p4.Pt();
	  ThqPartonHistory->auxdecor< float >("MC_W_decay2_from_Tau2_phi") = tH.W_decay2_from_Tau2_p4.Phi();
	  ThqPartonHistory->auxdecor< int >("MC_W_decay2_from_Tau2_pdgId") = tH.W_decay2_from_Tau2_pdgId;
	  fillEtaBranch(ThqPartonHistory, "MC_W_decay2_from_Tau2_eta", tH.W_decay2_from_Tau2_p4);
	
	//Hadr. Variables
	  ThqPartonHistory->auxdecor< int >("MC_hadr_Tau_Jet1") = tH.TauJets1;
	  ThqPartonHistory->auxdecor< int >("MC_hadr_Tau_Jet2") = tH.TauJets2;
	}

	if(event_HiggsWW) {

	  //W+ (WbosonP)
	  
	  //Lepton from first W
	  ThqPartonHistory->auxdecor< float >("MC_W_decay1_from_W1_m") = tH.W_decay1_from_W1_p4.M();
	  ThqPartonHistory->auxdecor< float >("MC_W_decay1_from_W1_pt") = tH.W_decay1_from_W1_p4.Pt();
	  ThqPartonHistory->auxdecor< float >("MC_W_decay1_from_W1_phi") = tH.W_decay1_from_W1_p4.Phi();
	  ThqPartonHistory->auxdecor< int >("MC_W_decay1_from_W1_pdgId") = tH.W_decay1_from_W1_pdgId;
	  fillEtaBranch(ThqPartonHistory, "MC_W_decay1_from_W1_eta", tH.W_decay1_from_W1_p4);

	  //Neutrino from W+
	  ThqPartonHistory->auxdecor< float >("MC_W_decay2_from_W1_m") = tH.W_decay2_from_W1_p4.M();
	  ThqPartonHistory->auxdecor< float >("MC_W_decay2_from_W1_pt") = tH.W_decay2_from_W1_p4.Pt();
	  ThqPartonHistory->auxdecor< float >("MC_W_decay2_from_W1_phi") = tH.W_decay2_from_W1_p4.Phi();
	  ThqPartonHistory->auxdecor< int >("MC_W_decay2_from_W1_pdgId") = tH.W_decay2_from_W1_pdgId;
	  fillEtaBranch(ThqPartonHistory, "MC_W_decay2_from_W1_eta", tH.W_decay2_from_W1_p4);

	  //W- (WbosonN)

	  //Lepton from W-
	  ThqPartonHistory->auxdecor< float >("MC_W_decay1_from_W2_m") = tH.W_decay1_from_W2_p4.M();
	  ThqPartonHistory->auxdecor< float >("MC_W_decay1_from_W2_pt") = tH.W_decay1_from_W2_p4.Pt();
	  ThqPartonHistory->auxdecor< float >("MC_W_decay1_from_W2_phi") = tH.W_decay1_from_W2_p4.Phi();
	  ThqPartonHistory->auxdecor< int >("MC_W_decay1_from_W2_pdgId") = tH.W_decay1_from_W2_pdgId;
	  fillEtaBranch(ThqPartonHistory, "MC_W_decay1_from_W2_eta", tH.W_decay1_from_W2_p4);

	  //Neutrino from W-
	  ThqPartonHistory->auxdecor< float >("MC_W_decay2_from_W2_m") = tH.W_decay2_from_W2_p4.M();
	  ThqPartonHistory->auxdecor< float >("MC_W_decay2_from_W2_pt") = tH.W_decay2_from_W2_p4.Pt();
	  ThqPartonHistory->auxdecor< float >("MC_W_decay2_from_W2_phi") = tH.W_decay2_from_W2_p4.Phi();
	  ThqPartonHistory->auxdecor< int >("MC_W_decay2_from_W2_pdgId") = tH.W_decay2_from_W2_pdgId;
	  fillEtaBranch(ThqPartonHistory, "MC_W_decay2_from_W2_eta", tH.W_decay2_from_W2_p4);
	}
	
	if(event_HiggsZZ) {

	  //First Z boson
	  if(Number_HiggsZZ == 1 || Number_HiggsZZ == 3) {
	    //Lepton 1 from first Z
	    ThqPartonHistory->auxdecor< float >("MC_Z_Lepton1_from_Z1_m") = tH.Z_Lepton1_from_Z1_p4.M();
	    ThqPartonHistory->auxdecor< float >("MC_Z_Lepton1_from_Z1_pt") = tH.Z_Lepton1_from_Z1_p4.Pt();
	    ThqPartonHistory->auxdecor< float >("MC_Z_Lepton1_from_Z1_phi") = tH.Z_Lepton1_from_Z1_p4.Phi();
	    ThqPartonHistory->auxdecor< int >("MC_Z_Lepton1_from_Z1_pdgId") = tH.Z_Lepton1_from_Z1_pdgId;
	    fillEtaBranch(ThqPartonHistory, "MC_Z_Lepton1_from_Z1_eta", tH.Z_Lepton1_from_Z1_p4);

	    //Lepton 2 from first Z
	    ThqPartonHistory->auxdecor< float >("MC_Z_Lepton2_from_Z1_m") = tH.Z_Lepton2_from_Z1_p4.M();
	    ThqPartonHistory->auxdecor< float >("MC_Z_Lepton2_from_Z1_pt") = tH.Z_Lepton2_from_Z1_p4.Pt();
	    ThqPartonHistory->auxdecor< float >("MC_Z_Lepton2_from_Z1_phi") = tH.Z_Lepton2_from_Z1_p4.Phi();
	    ThqPartonHistory->auxdecor< int >("MC_Z_Lepton2_from_Z1_pdgId") = tH.Z_Lepton2_from_Z1_pdgId;
	    fillEtaBranch(ThqPartonHistory, "MC_Z_Lepton2_from_Z1_eta", tH.Z_Lepton2_from_Z1_p4);
	  }

	  //Second Z boson
	  if(Number_HiggsZZ == 2 || Number_HiggsZZ == 3) {
	    //Lepton 1 from second Z
	    ThqPartonHistory->auxdecor< float >("MC_Z_Lepton1_from_Z2_m") = tH.Z_Lepton1_from_Z2_p4.M();
	    ThqPartonHistory->auxdecor< float >("MC_Z_Lepton1_from_Z2_pt") = tH.Z_Lepton1_from_Z2_p4.Pt();
	    ThqPartonHistory->auxdecor< float >("MC_Z_Lepton1_from_Z2_phi") = tH.Z_Lepton1_from_Z2_p4.Phi();
	    ThqPartonHistory->auxdecor< int >("MC_Z_Lepton1_from_Z2_pdgId") = tH.Z_Lepton1_from_Z2_pdgId;
	    fillEtaBranch(ThqPartonHistory, "MC_Z_Lepton1_from_Z2_eta", tH.Z_Lepton1_from_Z2_p4);

	    //Lepton  from second Z
	    ThqPartonHistory->auxdecor< float >("MC_Z_Lepton2_from_Z2_m") = tH.Z_Lepton2_from_Z2_p4.M();
	    ThqPartonHistory->auxdecor< float >("MC_Z_Lepton2_from_Z2_pt") = tH.Z_Lepton2_from_Z2_p4.Pt();
	    ThqPartonHistory->auxdecor< float >("MC_Z_Lepton2_from_Z2_phi") = tH.Z_Lepton2_from_Z2_p4.Phi();
	    ThqPartonHistory->auxdecor< int >("MC_Z_Lepton2_from_Z2_pdgId") = tH.Z_Lepton2_from_Z2_pdgId;
	    fillEtaBranch(ThqPartonHistory, "MC_Z_Lepton2_from_Z2_eta", tH.Z_Lepton2_from_Z2_p4);
	    /////
	  }
	}

      }
    }
  }

  StatusCode CalcThqPartonHistory::execute() {
    //Get the Truth Particles
    const xAOD::TruthParticleContainer* truthParticles(nullptr);

    ATH_CHECK(evtStore()->retrieve(truthParticles, m_config->sgKeyMCParticle()));

    // Create the partonHistory xAOD object
    xAOD::PartonHistoryAuxContainer* partonAuxCont = new xAOD::PartonHistoryAuxContainer {};
    xAOD::PartonHistoryContainer* partonCont = new xAOD::PartonHistoryContainer {};
    partonCont->setStore(partonAuxCont);

    xAOD::PartonHistory* ThqPartonHistory = new xAOD::PartonHistory {};
    partonCont->push_back(ThqPartonHistory);

    // Recover the parton history for TH events
    THHistorySaver(truthParticles, ThqPartonHistory);

    // Save to StoreGate / TStore
    std::string outputSGKey = m_config->sgKeyTopPartonHistory();
    std::string outputSGKeyAux = outputSGKey + "Aux.";

    xAOD::TReturnCode save = evtStore()->tds()->record(partonCont, outputSGKey);
    xAOD::TReturnCode saveAux = evtStore()->tds()->record(partonAuxCont, outputSGKeyAux);
    if (!save || !saveAux) {
      return StatusCode::FAILURE;
    }

    return StatusCode::SUCCESS;
  }
}
